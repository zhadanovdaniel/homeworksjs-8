
// Опишіть своїми словами що таке Document Object Model (DOM)
// Document Object Model (DOM) - це програмний інтерфейс, який надає доступ до структури і контенту веб-сторінки. 
// Він представляє HTML-документ у вигляді дерева об'єктів, де кожен елемент сторінки відповідає окремому об'єкту.
// DOM дозволяє змінювати структуру, стилі та зміст веб-сторінки за допомогою JavaScript.

// Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// Різниця між властивостями HTML-елементів innerHTML та innerText полягає в тому, як вони повертають текстовий контент елемента.
// innerHTML повертає HTML-код, який міститься в середині елемента.
//  Це включає всі HTML-теги та їх вміст, що знаходяться всередині елемента. 
//  Наприклад, якщо викликати element.innerHTML, існуючий елемент <div> з вмістом <p>Hello,
//   <strong>world!</strong></p>, то повернеться рядок "<p>Hello, <strong>world!</strong></p>".

// innerText повертає лише видимий текстовий контент елемента, виключаючи HTML-теги. 
// Він враховує тільки текстовий вміст елемента, не звертаючи уваги на будь-які вкладені елементи або їх стилі. 
// Наприклад, для елемента <div> з вмістом <p>Hello, <strong>world!</strong></p>, element.innerText поверне рядок "Hello, world!".

// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

// Щодо доступу до елемента сторінки за допомогою JavaScript, існують різні методи. Основні способи звернення до елемента:

// За допомогою ідентифікатора (ID): Можна надати елементу унікальний ідентифікатор (id) у HTML-коді, 
// а потім використовувати document.getElementById('elementId') для отримання посилання на нього. Цей метод найкраще використовувати, 
// якщо вам потрібно звернутися до одного конкретного елемента.

// За допомогою класу (Class): Якщо ви хочете звернутися до елемента (або групи елементів) з певним класом, 
// ви можете використовувати document.getElementsByClassName('className'). Цей метод поверне колекцію елементів, 
// які мають заданий клас.

// За допомогою тегу (Tag): Якщо ви хочете звернутися до всіх елементів певного типу (наприклад, всіх елементів <div>), 
// ви можете використовувати document.getElementsByTagName('tagName'). Цей метод також повертає колекцію елементів.

// Кращий спосіб залежить від конкретних потреб вашої задачі. Якщо вам потрібно звернутися до конкретного елемента, 
// рекомендується використовувати ідентифікатор (ID). Якщо вам потрібно звернутися до групи елементів з однаковим класом,
//  використовуйте клас (Class). Якщо вам потрібно звернутися до всіх елементів певного типу, використовуйте тег (Tag).


let list = document.getElementsByTagName('p');
for(let i = 0; i < list.length; i++) {
    list[i].style.backgroundColor = '#ff0000'
};//Знайти всі параграфи на сторінці та встановити колір фону #ff0000

let optionsList = document.getElementById('optionsList');//Знайти елемент із id="optionsList".
console.log(optionsList ); //Вивести у консоль.
console.log(optionsList.parentElement);//Знайти батьківський елемент та вивести в консоль. 
if(optionsList.hasChildNodes){
    
    console.log(optionsList.childNodes);//Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
    for (var i = 0; i < optionsList.childNodes.length; ++i) {
       console.log( optionsList.childNodes[i].nodeType)
       console.log( optionsList.childNodes[i].nodeName)
}};
//Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
let par = document.getElementById('testParagraph');
    par.textContent = 'This is a paragraph';
    console.log(par);
 
    //Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
    let mainHeader = document.querySelector('.main-header');
    let elements = mainHeader.querySelectorAll('*');
     elements.forEach(function(element) {
        element.classList.add('nav-item');
        console.log(element);
      });

      //Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
      let section = document.querySelectorAll('.section-title')
      section.forEach(function(element) {
        element.classList.remove('section-title');
        console.log(element);
      });